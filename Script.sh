@echo off
setlocal enabledelayedexpansion

REM Defina a versão desejada do Gradle
set "GRADLE_VERSION=8.3"

REM Defina o diretório de destino para o Gradle
set "GRADLE_DIR=%USERPROFILE%\gradle"

REM Crie o diretório se não existir
if not exist "%GRADLE_DIR%" mkdir "%GRADLE_DIR%"

REM Baixe o Gradle
echo Baixando Gradle %GRADLE_VERSION%...
bitsadmin /transfer "DownloadGradle" https://services.gradle.org/distributions/gradle-%GRADLE_VERSION%-bin.zip "%GRADLE_DIR%\gradle.zip"

REM Verifique se o download foi bem-sucedido
if not exist "%GRADLE_DIR%\gradle.zip" (
    echo Erro ao baixar o Gradle.
    exit /b 1
)

REM Extraia o Gradle
echo Extraindo Gradle...
powershell -command "Expand-Archive -Path '%GRADLE_DIR%\gradle.zip' -DestinationPath '%GRADLE_DIR%'"

REM Remova o arquivo zip após a extração
del "%GRADLE_DIR%\gradle.zip"

REM Configure a variável de ambiente GRADLE_HOME
setx GRADLE_HOME "%GRADLE_DIR%\gradle-%GRADLE_VERSION%"

REM Adicione Gradle ao PATH
setx PATH "%PATH%;%GRADLE_DIR%\gradle-%GRADLE_VERSION%\bin"

REM Verifique a instalação
echo Gradle instalado e configurado com sucesso. Versão:
gradle -v

pause