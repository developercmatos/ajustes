# Atualização do Projeto no GitLab

Este tutorial descreve como atualizar o nome do projeto, o path do projeto e o grupo do projeto no GitLab, bem como atualizar essas mudanças no repositório local sem a necessidade de um novo clone.

## 1. Atualizando o Nome e o Path do Projeto no GitLab

1. Acesse o GitLab e faça login na sua conta.
2. Navegue até o projeto que você deseja renomear.
3. No menu lateral, clique em **Settings** e depois em **General**.
4. Role a página até a seção **Advanced**.
5. Em **Rename repository**, atualize os seguintes campos:
    - **Project name**: Digite o novo nome do projeto.
    - **Path**: Atualize o path do projeto. Este é o caminho que aparecerá na URL do repositório.
6. Clique em **Rename project** para salvar as alterações.

## 2. Atualizando o Grupo do Projeto no GitLab

1. Acesse o GitLab e faça login na sua conta.
2. Navegue até o projeto que você deseja mover para um novo grupo.
3. No menu lateral, clique em **Settings** e depois em **General**.
4. Role a página até a seção **Advanced**.
5. Em **Transfer project**, selecione o novo grupo no campo **Select a new namespace**.
6. Clique em **Transfer project** para confirmar a transferência.

## 3. Atualizando Localmente

Se o projeto já foi clonado e você não quer fazer um novo clone, você precisará atualizar a URL do repositório remoto no seu repositório local.

1. Abra um terminal ou prompt de comando.
2. Navegue até o diretório do seu repositório local.
    ```bash
    cd /caminho/para/seu/projeto
    ```
3. Verifique a URL atual do repositório remoto.
    ```bash
    git remote -v
    ```
    Você verá algo como:
    ```plaintext
    origin  https://gitlab.com/old-group/old-project-path.git (fetch)
    origin  https://gitlab.com/old-group/old-project-path.git (push)
    ```
4. Atualize a URL do repositório remoto com o novo caminho do repositório.
    ```bash
    git remote set-url origin https://gitlab.com/new-group/new-project-path.git
    ```
5. Verifique se a URL foi atualizada corretamente.
    ```bash
    git remote -v
    ```
    Você deve ver algo como:
    ```plaintext
    origin  https://gitlab.com/new-group/new-project-path.git (fetch)
    origin  https://gitlab.com/new-group/new-project-path.git (push)
    ```
6. Teste se as configurações estão funcionando corretamente puxando as últimas mudanças do repositório remoto.
    ```bash
    git pull
    ```

## Conclusão

Com esses passos, você pode atualizar o nome do seu projeto, o path do projeto e mudar o grupo ao qual ele pertence no GitLab. Além disso, você ajusta o repositório local para refletir essas mudanças sem a necessidade de um novo clone. Isso ajuda a manter o fluxo de trabalho sem interrupções significativas.
