
# Tutorial Básico de Uso do Cypress

## Índice

- [Sobre a Ferramenta](#sobre-a-ferramenta)
- [Instalação](#instalacao)
  - [Instalação do Node.js](#instalacao-do-nodejs)
  - [Instalação do Cypress](#instalacao-do-cypress)
- [Automação Web com a Lojinha](#automacao-web-com-a-lojinha)
  - [Criação de Testes de Login](#criacao-de-testes-de-login)
- [Automação de Testes de API](#automacao-de-testes-de-api)
  - [Configuração de Comandos Customizados](#configuracao-de-comandos-customizados)
  - [Criação de Teste de Contrato](#criacao-de-teste-de-contrato)
- [Execução dos Testes](#execucao-dos-testes)
- [Geração de Relatórios](#geracao-de-relatorios)
- [Configuração de Pipeline](#configuracao-de-pipeline)
- [Variáveis de Ambiente](#variaveis-de-ambiente)
- [Scripts de Linha de Comando](#scripts-de-linha-de-comando)
- [Links de Referência](#links-de-referencia)
- [Contatos do Grupo](#contatos-do-grupo)
- [Objetivo do Tutorial](#objetivo-do-tutorial)

## Sobre a Ferramenta

Cypress é uma ferramenta de teste de interface construída para a web moderna. Ela permite que testadores e desenvolvedores escrevam testes rápidos, fáceis e confiáveis para aplicações web. Com uma arquitetura única, o Cypress é capaz de rodar na mesma execução do aplicativo que está sendo testado, oferecendo vantagens significativas em termos de velocidade e depuração.

## Instalação

### Instalação do Node.js

1. **Download do Node.js**:

   - Acesse o site oficial: [Node.js](https://nodejs.org/)
   - Baixe a versão LTS recomendada para sua plataforma.

2. **Instalação**:

   - Siga as instruções do instalador.

3. **Verificação da Instalação**:
   - Abra o terminal ou prompt de comando e execute:
     ```bash
     node -v
     npm -v
     ```
   - Verifique se as versões do Node.js e npm foram instaladas corretamente.

### Instalação do Cypress

1. **Inicialize um Projeto Node.js**:

   - No terminal, navegue até a pasta do seu projeto e execute:
     ```bash
     npm init -y
     ```
   - Isso criará um arquivo `package.json` com as configurações básicas do projeto.

2. **Instale o Cypress**:

   - Execute o comando abaixo para instalar o Cypress como dependência de desenvolvimento:
     ```bash
     npm install cypress --save-dev
     ```

3. **Abrindo o Cypress pela Primeira Vez**:
   - Execute o comando para abrir a interface gráfica do Cypress:
     ```bash
     npx cypress open
     ```
   - Selecione a opção "E2E testing";
   - Clique em "Continuar";
   - Selecione o navegador para realização dos testes;
   - Clique no botão "Start E2E testing";
   - Selecione a opção "Create new spec" e clique no botão "Create spec";
   - Clique no botão "Okay, run the spec".
   
  - O cypress deve abrir de forma automatizada o site https://example.cypress.io/.

## Automação Web com a Lojinha

Crie na raiz do projeto o arquivo `cypress.env.json` e dentro dele, adicione o conteudo abaixo:

```json
{
  "username": "seu usuario da lojinha",
  "password": "sua senha da lojinha"
}
```

Nota: Esse arquivo deve ser mantido localmente e não deve ser incluído no repositório para proteger informações sensíveis.

Adicione o seguinte código ao arquivo cypress.config.js.

```javascript
const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
        baseUrl: "http://165.227.93.41",

    setupNodeEvents(on, config) {
    },
  },
});
```

### Criação de Testes de Login

1. **Teste de Login com Falha**:

   - Crie um arquivo `login_fail_lojinha.cy.js` dentro da pasta `cypress/e2e` e adicione o seguinte código:
     ```javascript
     describe("Teste de login com falha na Lojinha.", () => {
       it("Login - Fail", () => {
         cy.visit("/lojinha-web/v2/").wait(1000);
         cy.get(":nth-child(2) > .input-field > label").type(
           Cypress.env("username")
         );
         cy.get(":nth-child(3) > .input-field > label").type("admin2");
         cy.get("#btn-entrar").click().wait(1000);
         cy.get(".toast").should("be.visible");
         cy.get("#toast-container .toast").should(
           "contain.text",
           "Falha ao fazer o login"
         );
       });
     });
     ```

2. **Teste de Login com Sucesso**:

   - Crie um arquivo `login_sucess_lojinha.cy.js` dentro da pasta `cypress/e2e` e adicione o seguinte código:
     ```javascript
     describe("Teste de login com sucesso na lojinha.", () => {
       it("Login - Success", () => {
         cy.visit("/lojinha-web/v2/");
         cy.get(":nth-child(2) > .input-field > label").type(
           Cypress.env("username")
         );
         cy.get(":nth-child(3) > .input-field > label").type(
           Cypress.env("password")
         );
         cy.get("#btn-entrar").click();
         cy.location("pathname").should("equal", "/lojinha-web/v2/produto");
       });
     });
     ```

3. **Teste de criação de um produto**:

   - Crie um arquivo `adicionar_produto.cy.js` dentro da pasta `cypress/e2e` e adicione o seguinte código:
     ```javascript
      describe('Teste de criação de um produto na lojinha.', () => {
        it('Adicionar produto e ler em lista de produtos.', () => {
          cy.visit("/lojinha-web/v2/");
          cy.get(":nth-child(2) > .input-field > label").type(
            Cypress.env("username")
          );
          cy.get(":nth-child(3) > .input-field > label").type(
            Cypress.env("password")
          );
          cy.get('#btn-entrar').click();
          cy.get('.waves-effect').click();
          cy.get('#produtonome').type('Teste');
          cy.get('#produtovalor').type('10,00');
          cy.get('#produtocores').type('Branco');
          cy.get('#btn-salvar').click().wait(1000);
          cy.get('.toast').should('be.visible');
          cy.get('#toast-container .toast').should('contain.text', 'Produto adicionado com sucesso');
          cy.get(':nth-child(4) > .grey').click();
          cy.get('.avatar span a').last().should('contain.text','Teste');
          cy.get('.avatar p').last().should('contain.text','10,00');
        });
      });
     ```

4. **Execução dos Testes Criados**:
   - Certifique-se de ter atualizado o valor das variáveis no arquivo cypress.env.json com as credenciais do seu usuário de teste;
   - Execute os testes com o comando abaixo:

     ```bash
     npx cypress run
     ```

## Automação de Testes de API

### Instalação de Dependências

Antes de prosseguir, instale as dependências necessárias para a geração de relatórios e validação de contrato:

```bash
npm install cypress-mochawesome-reporter@^3.8.2 cypress-multi-reporters@^1.6.4 jsonschema@^1.4.1 mocha-junit-reporter@^2.2.1 mochawesome@^7.1.3 mochawesome-merge@^4.3.0 mochawesome-report-generator@^6.2.0 --save-dev
```

### Configuração de Comandos Customizados

1. **Criação de Comando Customizado para Obter Token**:
   - No arquivo `cypress/support/commands.js`, adicione o seguinte código para gerar tokens de autenticação:
     ```javascript
     Cypress.Commands.add("getAccessToken", () => {
       cy.request({
         method: "POST",
         url: "/lojinha/v2/login",
         body: {
           usuarioLogin: Cypress.env("usuarioLogin"),
           usuarioSenha: Cypress.env("usuarioSenha"),
         },
       }).then((response) => {
         Cypress.env("accessToken", response.body.data.token);
       });
     });
     ```
2. **Criação do Schema JSON**:
   - Crie a pasta schemas dentro do diretorio `cypress/fixtures` e em seguida crie um arquivo json dentro da pasta schema chamado `contrato-get-produtos.json` e adicione dentro dele o conteudo abaixo:

   ```json
   {
  "$schema": "https://json-schema.org/draft-07/schema",
  "$id": "http://example.com/example.json",
  "title": "Root Schema",
  "type": "object",
  "required": ["data", "message", "error"],
  "properties": {
    "data": {
      "type": "array",
      "items": {
        "type": "object",
        "required": [
          "produtoId",
          "produtoNome",
          "produtoValor",
          "produtoCores",
          "produtoUrlMock",
          "componentes"
        ],
        "properties": {
          "produtoId": {
            "type": "integer"
          },
          "produtoNome": {
            "type": "string"
          },
          "produtoValor": {
            "type": "integer"
          },
          "produtoCores": {
            "type": "array",
            "items": {
              "type": "string"
            }
          },
          "produtoUrlMock": {
            "type": "string"
          },
          "componentes": {
            "type": "array",
            "items": {
              "type": "object",
              "required": ["componenteId", "componenteNome"],
              "properties": {
                "componenteId": {
                  "type": "integer"
                },
                "componenteNome": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "message": {
      "type": "string"
    },
    "error": {
      "type": "string"
    }
  }
}
   ```


### Criação de Teste de Contrato

1. **Criação do Teste de Contrato**:

   - Crie um arquivo `contrato_get_produtos_test.cy.js` dentro da pasta `cypress/e2e` e adicione o seguinte código:

     ```javascript
     const { Validator } = require("jsonschema");

     describe("Teste de Contrato da API", () => {
       let validator;

       before(() => {
         validator = new Validator();
         cy.getAccessToken();
       });

       it("deve validar o schema da API GET produtos", () => {
         cy.fixture("schemas/contrato-get-produtos.json").then((schema) => {
           cy.request({
             method: "GET",
             url: "/lojinha/v2/produtos",
             headers: {
               token: `${Cypress.env("accessToken")}`,
             },
           }).then((response) => {
             const validationResult = validator.validate(response.body, schema);
             expect(
               validationResult.valid,
               "A resposta segue o schema esperado"
             ).to.be.true;
             if (!validationResult.valid) {
               validationResult.errors.forEach((error) => {
                 console.error(`Erro: ${error.property} ${error.message}`);
               });
             }
           });
         });
       });
     });
     ```

## Execução dos Testes

- Para executar todos os testes, utilize o seguinte comando no terminal:
  ```bash
  npx cypress run
  ```

## Configuração e geração de Relatórios

No arquivo de configuração cypress.config.js, podemos configurar a geração de relatórios automaticamente. Adicione o código abaixo para configurar o report.

```javascript
module.exports = {
  e2e: {
    baseUrl: "http://165.227.93.41",
    reporter: "cypress-multi-reporters",
    reporterOptions: {
      configFile: "reporter-config.json",
    },
    setupNodeEvents(on, config) {
      require("cypress-mochawesome-reporter/plugin")(on);
      return config;
    },
  },
};
```

Adicione o arquivo `reporter-config.json` na raiz do projeto com o seguinte conteúdo:

```json
{
  "reporterEnabled": "mochawesome, mocha-junit-reporter",
  "mochawesomeReporterOptions": {
    "reportDir": "cypress/reports/html/.jsons",
    "reportFilename": "report",
    "overwrite": false,
    "html": true,
    "json": true
  },
  "mochaJunitReporterReporterOptions": {
    "mochaFile": "cypress/reports/junit/results-[hash].xml",
    "toConsole": true
  }
}
```

Para gerar relatórios durante a execução dos testes, utilize o seguinte comando:

```bash
npx cypress run --reporter mochawesome --reporter-options reportDir=cypress/reports,reportFilename=report
```

## Scripts de Linha de Comando

Para facilitar a execução dos testes via linha de comando, adicione o seguinte script ao seu package.json:

```json
"scripts": {
  "test": "npx cypress run --reporter mochawesome --reporter-options reportDir=cypress/reports,reportFilename=report"
}
```

Agora, você pode executar todos os testes com a geração de relatórios utilizando o comando:

```bash
npm run test
```
